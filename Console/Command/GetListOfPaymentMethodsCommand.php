<?php

namespace KodetIo\ListMethods\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetListOfPaymentMethodsCommand extends Command
{

    /** @var State */
    private $state;

    /** @var ScopeConfigInterface */
    private $scope;

    /**
     * @param State $state
     * @param ScopeConfigInterface $scope
     * @param string|null $name
     */
    public function __construct(
        State                $state,
        ScopeConfigInterface $scope,
        string               $name = null
    )
    {
        $this->state = $state;
        $this->scope = $scope;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('payment:list:methods');
        $this->setDescription('Exports list of payment methods');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->state->setAreaCode(Area::AREA_GLOBAL);
        return $this->showAllPaymentMethodList($output);
    }

    /**
     * @param OutputInterface $output
     * @return int
     */
    private function showAllPaymentMethodList(OutputInterface $output): int
    {
        try {
            $table = new Table($output);
            $table->setHeaders(['Code', 'Name', 'Status']);
            $listOfPaymentMethods = $this->scope->getValue('payment');

            foreach ($listOfPaymentMethods as $paymentCode => $paymentMethod) {
                $status = isset($paymentMethod['active']) ? ($paymentMethod['active'] == 1 ? 'Enabled' : 'Disabled') : '-';
                $title = $paymentMethod['title'] ?? '-';
                $this->addRowToTable($table, [$paymentCode, $title, $status]);
            }

            $table->render();

            return Cli::RETURN_SUCCESS;
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $output->writeln($e->getTraceAsString());
            }
            return Cli::RETURN_FAILURE;
        }
    }

    /**
     * @param Table $table
     * @param array $args
     * @return void
     */
    private function addRowToTable(Table $table, array $args): void
    {
        $table->addRow($args);
    }
}
