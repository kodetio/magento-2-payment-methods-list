# Magento 2 list of all payment & shipping methods module

Solves simple issue Magento 2 developers & admins are facing: get list of all payment methods.

## Installation

Download the package using composer and update Magento 2 generated files:

```bash
composer require kodet.io/magento2-payment-methods-list
bin/magento setup:upgrade
bin/magento setup:di:c
```

## Usage

```bash
bin/magento payment:list:methods
```

which generates output like this:

```bash
+----------------------------+---------------------------------------------+----------+
| Code                       | Name                                        | Status   |
+----------------------------+---------------------------------------------+----------+
| free                       | No Payment Information Required             | Enabled  |
| substitution               | -                                           | Disabled |
| checkmo                    | Check / Money order                         | Disabled |
| purchaseorder              | Purchase Order                              | Disabled |
| vault                      | -                                           | -        |
| paypal_express             | PayPal Express Checkout                     | Disabled |
| paypal_express_bml         | PayPal Credit (Paypal Express Bml)          | -        |
| payflow_express            | PayPal Express Checkout Payflow Edition     | -        |
| payflow_express_bml        | PayPal Credit (Payflow Express Bml)         | -        |
| payflowpro                 | Credit Card (Payflow Pro)                   | -        |
| payflowpro_cc_vault        | Stored Cards (Payflow Pro)                  | -        |
| paypal_billing_agreement   | PayPal Billing Agreement                    | Enabled  |
| payflow_link               | Credit Card (Payflow Link)                  | -        |
| payflow_advanced           | Credit Card (Payflow Advanced)              | -        |
| hosted_pro                 | Payment by cards or by PayPal account       | -        |
| amazon_payment             | Amazon Pay                                  | Disabled |
| amazonlogin                | Amazon Pay                                  | Disabled |
| braintree_paypal           | PayPal                                      | Disabled |
| braintree_paypal_credit    | PayPal Credit                               | Enabled  |
| braintree_cc_vault         | Stored Cards                                | Disabled |
| braintree_paypal_vault     | Stored Accounts (PayPal)                    | Disabled |
| braintree_applepay         | Apple Pay                                   | Disabled |
| braintree_googlepay        | Google Pay                                  | Disabled |
| braintree_venmo            | Venmo                                       | Disabled |
| braintree_ach_direct_debit | ACH Direct Debit                            | Disabled |
| braintree_local_payment    | Local Payments                              | Disabled |
+----------------------------+---------------------------------------------+----------+
```

```bash
bin/magento shipping:list:methods
```

which generates output like this:

```bash
+-------------------------+------------------------------+----------+
| Code                    | Name                         | Status   |
+-------------------------+------------------------------+----------+
| dhl                     | DHL                          | Disabled |
| fedex                   | Federal Express              | Disabled |
| instore                 | In-Store Pickup Delivery     | Disabled |
| ups                     | United Parcel Service        | Disabled |
| usps                    | United States Postal Service | Disabled |
| amstrates               | Shipping Table Rates         | Enabled  |
+-------------------------+------------------------------+----------+

```
